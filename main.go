package main

import (
	"log"
	"net/http"

	"bitbucket.org/nazarnovak/login/app"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	app := app.NewApp()

	http.Handle("/", app.Server())
	http.Handle("/favicon.ico", http.NotFoundHandler())

	log.Printf("Listening on port %s", app.Config.Port)
	err := http.ListenAndServe(":"+ app.Config.Port, nil)
	if err != nil {
		panic(err)
	}

}
