package app

import (
	"encoding/json"
	"flag"
	"io/ioutil"
)

type Config struct {
	Mysql `json:"mysql"`
	Email `json:"email"`
	Host string `json:"host"`
	Port string
}

type Mysql struct {
	User string `json:"user"`
	Password string `json:"password"`
	DB string `json:"db"`
}

type Email struct {
	Server string `json:"server"`
	User string `json:"user"`
	Password string `json:"password"`
	Subject string `json:"subject"`
}

func NewConfig() *Config {
	port := flag.String("port", "8080", "Server port")
	cFile := flag.String("conf", "config.json", "Config file")
	flag.Parse()

	raw, err := ioutil.ReadFile(*cFile)
	if err != nil {
		panic(err)
	}

	c := &Config{}
	json.Unmarshal(raw, c)

	c.Port = *port

	return c
}