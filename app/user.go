package app

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"

	"bitbucket.org/nazarnovak/login/models"
	"golang.org/x/crypto/bcrypt"
)

type JsonResponse struct {
	Error string `json:"error"`
}

func (a *App) UserHandlers(s *http.ServeMux) *http.ServeMux {
	s.HandleFunc("/user/create", a.UserCreate)
	s.HandleFunc("/user/read", a.UserRead)
	s.HandleFunc("/user/update", a.UserUpdate)

	return s
}

func (a *App) UserCreate(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	decoder := json.NewDecoder(r.Body)
	u := &models.User{}
	err := decoder.Decode(&u)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if (u.Email == "" || u.Password == "") && !u.GoogleAPI {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Email and password are required parameters"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	match, err := regexp.MatchString(`([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+`, u.Email)
	if err != nil || !match {
		if err != nil {
			log.Println(err)
		}
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Bad email"}
		json.NewEncoder(w).Encode(resp)
		return
	}



	if len(u.Password) < 4 && u.Password != "" {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Password must be at least 4 characters"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	models.SetDB(a.DB)
	var eu *models.User

	if u.GoogleAPI {
		eu, err = models.GetUserByEmailGoogle(u.Email)
	} else {
		eu, err = models.GetUserByEmail(u.Email)
	}

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{http.StatusText(http.StatusInternalServerError)}
		json.NewEncoder(w).Encode(resp)
		return
	}
	if eu != nil && !u.GoogleAPI {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Email already taken"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	if u.GoogleAPI && eu != nil {
		u = eu
	}

	if u.Id != 0 {
		err = u.Update()
	} else {
		err = u.Save()
	}

	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)

	json.NewEncoder(w).Encode(u)
}

func (a *App) UserRead(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	decoder := json.NewDecoder(r.Body)

	u := &models.User{}
	err := decoder.Decode(&u)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if u.Email == "" || u.Password == "" {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Email and password are required parameters"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	models.SetDB(a.DB)
	eu, err := models.GetUserByEmailPass(u.Email)

	if err != nil {
		log.Println(err)
		resp := &JsonResponse{http.StatusText(http.StatusInternalServerError)}
		json.NewEncoder(w).Encode(resp)
		return
	}
	if eu == nil {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Wrong email/username"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(eu.Password), []byte(u.Password))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Wrong email/username"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	w.WriteHeader(http.StatusOK)

	json.NewEncoder(w).Encode(eu)
}

func (a *App) UserUpdate(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	decoder := json.NewDecoder(r.Body)
	u := &models.User{}
	err := decoder.Decode(&u)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if u.Id == 0 || u.Email == "" || u.FullName ==  "" || u.Address == "" || u.Phone == "" {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Fields must not be empty"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	match, err := regexp.MatchString(`([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+`, u.Email)
	if err != nil || !match {
		if err != nil {
			log.Println(err)
		}
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Bad email"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	models.SetDB(a.DB)

	cu, err := models.GetUserById(u.Id)
	if err != nil {
		log.Println(err)
		resp := &JsonResponse{http.StatusText(http.StatusInternalServerError)}
		json.NewEncoder(w).Encode(resp)
		return
	}
	if cu == nil {
		log.Println(err)
		resp := &JsonResponse{http.StatusText(http.StatusBadRequest)}
		json.NewEncoder(w).Encode(resp)
		return
	}
	eu, err := models.GetUserByEmail(u.Email)
	if err != nil {
		log.Println(err)
		resp := &JsonResponse{http.StatusText(http.StatusInternalServerError)}
		json.NewEncoder(w).Encode(resp)
		return
	}
	if cu.Id != eu.Id && eu != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp := &JsonResponse{"Email already taken"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	err = u.Update()

	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(u)
}
