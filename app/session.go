package app

import (
	"net/http"

	"bitbucket.org/nazarnovak/login/models"
	"github.com/gorilla/sessions"
)

const COOKIE_NAME = "session"

type Session struct {
	Name     string
	Store    *sessions.CookieStore
	Users    map[string]*models.User
	Sessions map[string]string
}

func (a *App) NewSession() {
	a.Session = &Session{
		COOKIE_NAME,
		sessions.NewCookieStore([]byte("something-very-secret")),
		map[string]*models.User{},
		map[string]string{},
	}
	a.Session.Store.Options = &sessions.Options{
		//Domain:   a.Config.Host,
		Path:     "/",
		MaxAge:   3600 * 8, // 8 hours
		HttpOnly: true,
	}
}

func (s *Session) AddUser(w http.ResponseWriter, r *http.Request, u *models.User) error {
	usess, err := s.Store.Get(r, s.Name)
	if err != nil {
		return err
	}

	usess.Values["uid"] = u.Id
	usess.Values["uemail"] = u.Email
	usess.Values["ufullname"] = u.FullName
	usess.Values["uaddress"] = u.Address
	usess.Values["uphone"] = u.Phone
	usess.Values["ugoogleapi"] = u.GoogleAPI
	usess.Save(r, w)

	return nil
}

func (s *Session) GetUser(r *http.Request) (*models.User, error) {
	usess, err := s.Store.Get(r, s.Name)
	if err != nil {
		return nil, err
	}

	if usess.Values["uid"] == nil {
		return nil, nil
	}

	u := &models.User{
		usess.Values["uid"].(int),
		usess.Values["uemail"].(string),
		"",
		usess.Values["ufullname"].(string),
		usess.Values["uaddress"].(string),
		usess.Values["uphone"].(string),
		usess.Values["ugoogleapi"].(bool),
	}

	return u, nil

}

func (s *Session) DeleteUser(w http.ResponseWriter, r *http.Request) error {
	usess, err := s.Store.Get(r, s.Name)
	if err != nil {
		return err
	}

	delete(usess.Values, "uid")
	delete(usess.Values, "uemail")
	delete(usess.Values, "ufullname")
	delete(usess.Values, "uaddress")
	delete(usess.Values, "uphone")
	usess.Save(r, w)

	return nil
}
