package app

import (
	"database/sql"
)

type App struct {
	Config *Config
	DB      *sql.DB
	Session *Session
}

func NewApp() *App {
	a := &App{}
	a.Config = NewConfig()
	a.NewDB()
	a.NewSession()

	return a
}
