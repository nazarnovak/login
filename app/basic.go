package app

import (
	"net/http"
	"html/template"
	"log"

	"bitbucket.org/nazarnovak/login/models"
	"encoding/json"
	"bytes"
)

func (a *App) BasicHandlers (s *http.ServeMux) *http.ServeMux {
	s.HandleFunc("/", a.IndexHandler)
	s.HandleFunc("/login", a.LoginHandler)
	s.HandleFunc("/signup", a.SignupHandler)
	s.HandleFunc("/forgot", a.ForgotPasswordHandler)
	s.HandleFunc("/reset", a.ResetPasswordHandler)
	s.HandleFunc("/logout", a.LogoutHandler)
	s.HandleFunc("/setup", a.SetupHandler)
	s.HandleFunc("/setup/post", a.SetupPostHandler)
	s.HandleFunc("/profile", a.ProfileHandler)

	return s
}

func (a *App) IndexHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if  err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if u != nil {
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	http.Redirect(w, r, "/login", http.StatusSeeOther)
	return
}

func (a *App) LoginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if u != nil {
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	tmpl := template.Must(template.ParseFiles("public/html/layout.gohtml",
		"public/html/login.gohtml"))
	tmpl.Execute(w, nil)
}

func (a *App) SignupHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if u != nil {
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	tmpl := template.Must(template.ParseFiles("public/html/layout.gohtml",
		"public/html/signup.gohtml"))
	tmpl.Execute(w, nil)
}

func (a *App) ForgotPasswordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if u != nil {
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	tmpl := template.Must(template.ParseFiles("public/html/layout.gohtml",
		"public/html/forgot.gohtml"))
	tmpl.Execute(w, nil)
}

func (a *App) ResetPasswordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	_, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	r.ParseForm()
	if _, ok := r.Form[TOKEN]; !ok {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	token := r.Form[TOKEN][0]

	tmpl := template.Must(template.ParseFiles("public/html/layout.gohtml",
		"public/html/reset.gohtml"))
	tmpl.Execute(w, token)
}

func (a *App) LogoutHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	a.Session.DeleteUser(w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func (a *App) SetupHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if u == nil {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	if u.FullName != "" && u.Address != "" && u.Phone == "" {
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
		return
	}

	tmpl := template.Must(template.ParseFiles("public/html/layout.gohtml",
		"public/html/setup.gohtml"))

	tmpl.Execute(w, u)
}

func (a *App) SetupPostHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	r.ParseForm()
	var fields []string
	if _, ok := r.Form[FULLNAME]; !ok {
		fields = append(fields, FULLNAME)
	}
	if _, ok := r.Form[ADDRESS]; !ok {
		fields = append(fields, ADDRESS)
	}
	if _, ok := r.Form[EMAIL]; !ok {
		fields = append(fields, EMAIL)
	}
	if _, ok := r.Form[PHONE]; !ok {
		fields = append(fields, PHONE)
	}
	if _, ok := r.Form[GOOGLEAPI]; !ok {
		fields = append(fields, GOOGLEAPI)
	}
	if len(fields) != 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	u := &models.User{Email: r.Form[EMAIL][0], FullName: r.Form[FULLNAME][0],
		Address: r.Form[ADDRESS][0], Phone: r.Form[PHONE][0]}
	u.GoogleAPI = false
	if r.Form[GOOGLEAPI][0] == "true" {
		u.GoogleAPI = true
	}

	su, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	u.Id = su.Id
	data, err := json.Marshal(u)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	url := "http://" + r.Host + "/user/update"
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	apiResp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer apiResp.Body.Close()

	resp := &JsonResponse{}
	u = &models.User{}

	if apiResp.StatusCode != http.StatusBadRequest &&
		apiResp.StatusCode != http.StatusOK {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if apiResp.StatusCode == http.StatusBadRequest {
		err = json.NewDecoder(apiResp.Body).Decode(&resp)
	} else if apiResp.StatusCode == http.StatusOK {
		err = json.NewDecoder(apiResp.Body).Decode(&u)
	}

	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if apiResp.StatusCode == http.StatusBadRequest {
		json.NewEncoder(w).Encode(resp)
	} else if apiResp.StatusCode == http.StatusOK {
		a.Session.AddUser(w, r, u)
		jsonStr, err := json.Marshal(u)
		if err != nil {
			log.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.Write(jsonStr)
	}
}

func (a *App) ProfileHandler(w http.ResponseWriter, r *http.Request) {
	u, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if u == nil {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	if u.FullName == "" || u.Address == "" || u.Phone == "" {
		http.Redirect(w, r, "/setup", http.StatusSeeOther)
		return
	}

	tmpl := template.Must(template.ParseFiles("public/html/layout.gohtml",
		"public/html/profile.gohtml"))

	tmpl.Execute(w, u)
}
