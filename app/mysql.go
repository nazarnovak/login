package app

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func (a *App) NewDB() {
	var err error
	dsn := fmt.Sprintf("%s:%s@/%s?charset=utf8",a.Config.Mysql.User,
		a.Config.Mysql.Password, a.Config.Mysql.DB)
	a.DB, err = sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}

	err = a.DB.Ping()
	if err != nil {
		panic(err)
	}
}