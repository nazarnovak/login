package app

import (
	"net/http"
)

func (a *App) Server() *http.ServeMux {
	http.Handle("/css/", http.StripPrefix("/css", http.FileServer(http.Dir("public/css"))))
	http.Handle("/js/", http.StripPrefix("/js", http.FileServer(http.Dir("public/js"))))

	s := http.NewServeMux()

	a.BasicHandlers(s)
	a.AuthHandlers(s)
	a.UserHandlers(s)

	return s
}
