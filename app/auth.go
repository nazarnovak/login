package app

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/mail"
	"net/smtp"
	"regexp"

	"bitbucket.org/nazarnovak/login/models"
	"time"
)

const (
	EMAIL     = "email"
	PASS      = "pass"
	FULLNAME  = "fullname"
	GOOGLEAPI = "googleapi"
	ADDRESS   = "address"
	PHONE     = "phone"
	TOKEN     = "token"
)

func (a *App) AuthHandlers(s *http.ServeMux) *http.ServeMux {
	s.HandleFunc("/auth/login", a.AuthLoginHandler)
	s.HandleFunc("/auth/signup", a.AuthSignupHandler)
	s.HandleFunc("/auth/google", a.AuthGoogleHandler)
	s.HandleFunc("/auth/forgot", a.ForgotHandler)
	s.HandleFunc("/auth/reset", a.ResetHandler)

	return s
}

func (a *App) AuthLoginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if u != nil {
		http.Redirect(w, r, "/setup", http.StatusSeeOther)
		return
	}

	r.ParseForm()

	var fields []string
	if _, ok := r.Form[EMAIL]; !ok {
		fields = append(fields, EMAIL)
	}
	if _, ok := r.Form[PASS]; !ok {
		fields = append(fields, PASS)
	}
	if len(fields) != 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	u = &models.User{Email: r.Form[EMAIL][0], Password: r.Form[PASS][0]}
	data, err := json.Marshal(u)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	url := "http://" + r.Host + "/user/read"

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	apiResp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer apiResp.Body.Close()

	var resp interface{}

	if apiResp.StatusCode == http.StatusBadRequest {
		resp = &JsonResponse{}
		err = json.NewDecoder(apiResp.Body).Decode(&resp)
	} else if apiResp.StatusCode == http.StatusOK {
		u = &models.User{}
		err = json.NewDecoder(apiResp.Body).Decode(&u)
	} else {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if apiResp.StatusCode == http.StatusBadRequest {
		json.NewEncoder(w).Encode(resp)
		return
	}

	a.Session.AddUser(w, r, u)
	resp = &JsonResponse{""}
	jsonStr, err := json.Marshal(resp)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.Write(jsonStr)
}

func (a *App) AuthSignupHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil || u != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	r.ParseForm()

	var fields []string
	if _, ok := r.Form[EMAIL]; !ok {
		fields = append(fields, EMAIL)
	}
	if _, ok := r.Form[PASS]; !ok {
		fields = append(fields, PASS)
	}
	if len(fields) != 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	u = &models.User{Email: r.Form[EMAIL][0], Password: r.Form[PASS][0]}

	data, err := json.Marshal(u)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	url := "http://" + r.Host + "/user/create"

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	apiResp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer apiResp.Body.Close()

	var resp interface{}

	if apiResp.StatusCode == http.StatusBadRequest {
		resp = &JsonResponse{}
	} else if apiResp.StatusCode == http.StatusOK {
		u = &models.User{}
	} else {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if apiResp.StatusCode == http.StatusBadRequest {
		err = json.NewDecoder(apiResp.Body).Decode(&resp)
	} else if apiResp.StatusCode == http.StatusOK {
		err = json.NewDecoder(apiResp.Body).Decode(&u)
	}

	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if apiResp.StatusCode == http.StatusBadRequest {
		json.NewEncoder(w).Encode(resp)
	} else if apiResp.StatusCode == http.StatusOK {
		err = a.Session.AddUser(w, r, u)
		if err != nil {
			log.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		jsonStr, err := json.Marshal(u)
		if err != nil {
			log.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.Write(jsonStr)
	}
}

func (a *App) AuthGoogleHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil || u != nil {
		log.Println(err, u)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	r.ParseForm()

	var fields []string
	if _, ok := r.Form[EMAIL]; !ok {
		fields = append(fields, EMAIL)
	}
	if _, ok := r.Form[FULLNAME]; !ok {
		fields = append(fields, PASS)
	}

	if len(fields) != 0 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	u = &models.User{Email: r.Form[EMAIL][0], FullName: r.Form[FULLNAME][0]}

	u.GoogleAPI = true

	if _, ok := r.Form[GOOGLEAPI]; !ok ||
		r.Form[GOOGLEAPI][0] == "0" ||
		r.Form[GOOGLEAPI][0] == "false" {
		u.GoogleAPI = false
	}

	data, err := json.Marshal(u)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	url := "http://" + r.Host + "/user/create"

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	apiResp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer apiResp.Body.Close()

	var resp interface{}

	if apiResp.StatusCode == http.StatusBadRequest {
		resp = &JsonResponse{}
	} else if apiResp.StatusCode == http.StatusOK {
		u = &models.User{}
	} else {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if apiResp.StatusCode == http.StatusBadRequest {
		err = json.NewDecoder(apiResp.Body).Decode(&resp)
	} else if apiResp.StatusCode == http.StatusOK {
		err = json.NewDecoder(apiResp.Body).Decode(&u)
	}

	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if apiResp.StatusCode == http.StatusBadRequest {
		json.NewEncoder(w).Encode(resp)
	} else if apiResp.StatusCode == http.StatusOK {
		err = a.Session.AddUser(w, r, u)
		if err != nil {
			log.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		jsonStr, err := json.Marshal(u)
		if err != nil {
			log.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.Write(jsonStr)
	}
}

func (a *App) resetPasswordMail(email string) error {
	models.SetDB(a.DB)

	v, err := models.NewVerification(email)
	if err != nil {
		return err
	}

	from := mail.Address{"Login app demo", a.Config.Email.User}
	to := mail.Address{"", email}
	subj := a.Config.Email.Subject

	link := a.Config.Host + "/reset?token=" + v.Hash
	body := fmt.Sprintf(`<!DOCTYPE html>
<html>
<head><meta charset="UTF-8"><title>Email</title></head>
<body>
<p>Hello</p><p>You (someone) requested a password reset</p>
<p><a href="%s">Click here to reset the password</a></p>
</body>
</html>`, link)
	//body := fmt.Sprintf("Hello\nYou(someone) requested a password reset " +
	//	".\n\nTo reset it click on the link below\n\n%s", link)

	headers := make(map[string]string)
	headers["MIME-version"] = "1.0"
	headers["Content-Type"] = "text/html; charset=\"UTF-8\""
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = subj

	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	servername := a.Config.Email.Server

	host, _, _ := net.SplitHostPort(servername)
	auth := smtp.PlainAuth("", a.Config.Email.User, a.Config.Email.Password, host)

	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		log.Println(err)
		return err
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		log.Println(err)
		return err
	}

	if err = c.Auth(auth); err != nil {
		log.Println(err)
		return err
	}

	if err = c.Mail(from.Address); err != nil {
		log.Println(err)
		return err
	}

	if err = c.Rcpt(to.Address); err != nil {
		log.Println(err)
		return err
	}

	w, err := c.Data()
	if err != nil {
		log.Println(err)
		return err
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		log.Println(err)
		return err
	}

	err = w.Close()
	if err != nil {
		log.Println(err)
		return err
	}

	c.Quit()
	return nil
}

func (a *App) ForgotHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil || u != nil {
		log.Println(err, u)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	r.ParseForm()
	if _, ok := r.Form[EMAIL]; !ok {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	email := r.Form[EMAIL][0]

	match, err := regexp.MatchString(`([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+`, email)
	if err != nil || !match {
		if err != nil {
			log.Println(err)
		}
		resp := &JsonResponse{"Bad email"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	models.SetDB(a.DB)
	eu, err := models.GetUserByEmail(email)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if eu == nil {
		resp := &JsonResponse{"Bad email"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	if err = a.resetPasswordMail(email); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	resp := &JsonResponse{""}
	json.NewEncoder(w).Encode(resp)
}

func (a *App) ResetHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	u, err := a.Session.GetUser(r)
	if err != nil || u != nil {
		log.Println(err, u)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	r.ParseForm()
	if _, ok := r.Form[TOKEN]; !ok {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	if _, ok := r.Form[PASS]; !ok {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	token := r.Form[TOKEN][0]
	pass := r.Form[PASS][0]

	if pass == "" {
		resp := &JsonResponse{"Password must not be empty"}
		json.NewEncoder(w).Encode(resp)
		return
	}
	if len(pass) < 4 {
		resp := &JsonResponse{"Password must be at least 4 characters"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	models.SetDB(a.DB)
	v, err := models.GetVerificationByHash(token)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if v == nil {
		resp := &JsonResponse{"Bad token"}
		json.NewEncoder(w).Encode(resp)
		return
	}
	if v.Used {
		resp := &JsonResponse{"Token already used"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	expiration, err := time.Parse("2006-01-02 15:04:05", v.Expiration)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if time.Now().After(expiration.UTC()) {
		resp := &JsonResponse{"Token expired"}
		json.NewEncoder(w).Encode(resp)
		return
	}

	v.Update()

	uu, err := models.GetUserByEmail(v.Email)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	uu.Password = pass
	err = uu.Update()
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	resp := &JsonResponse{""}
	json.NewEncoder(w).Encode(resp)
	return
}
