package models

import (
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"strconv"
	"time"
)

type Verification struct {
	Id         int
	Email      string
	Hash       string
	Expiration string
	Used       bool
}

func NewVerification(email string) (*Verification, error) {
	h := md5.New()

	microtime := int(time.Now().UnixNano() / int64(time.Millisecond))
	salt := strconv.Itoa(microtime)
	io.WriteString(h, email+salt)

	hash := fmt.Sprintf("%x", h.Sum(nil))

	expiration := time.Now().UTC().Add(time.Minute * 15).Format("2006-01-02 15:04:05")

	v := &Verification{Email: email, Hash: hash, Expiration: expiration}
	err := v.Save()
	if err != nil {
		return nil, err
	}

	return v, nil
}

func (v *Verification) Save() error {
	stmt, err := db.Prepare("INSERT INTO `verifications`" +
		"SET email=?, hash=?, expiration=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(v.Email, v.Hash, v.Expiration)
	if err != nil {
		return err
	}

	return nil
}

func (v *Verification) Update() error {
	if v.Id == 0 {
		return errors.New("Cannot update non-existing record")
	}

	stmt, err := db.Prepare("UPDATE `verifications`" +
		"SET used='1'" +
		"WHERE id=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(v.Id)
	if err != nil {
		return err
	}

	return nil
}

func GetVerificationByHash(hash string) (*Verification, error) {
	v := &Verification{}

	row := db.QueryRow(fmt.Sprintf("SELECT id, email, hash, expiration, used "+
		"FROM `verifications` "+
		"WHERE hash='%s'"+
		"LIMIT 1", hash))

	var used string
	err := row.Scan(&v.Id, &v.Email, &v.Hash, &v.Expiration, &used)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	if used == "1" {
		v.Used = true
	}

	return v, nil
}
