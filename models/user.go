package models

import (
	"database/sql"
	"fmt"
	"errors"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id        int    `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	FullName  string `json:"fullname"`
	Address   string `json:"address"`
	Phone     string `json:"phone"`
	GoogleAPI bool   `json:"googleapi"`
}

func (u *User) Save() error {
	if u.Password != "" {
		hpass, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		if err != nil {
			return err
		}
		u.Password = string(hpass)
	}

	stmt, err := db.Prepare("INSERT INTO `users`" +
		"SET email=?, password=?, fullname=?, address=?, phone=?, " +
		"google_api=?")
	if err != nil {
		return err
	}

	gapi := "0"
	if u.GoogleAPI {
		gapi = "1"
	}

	res, err := stmt.Exec(u.Email, u.Password, u.FullName, u.Address,
		u.Phone, gapi)
	if err != nil {
		return err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	u.Id = int(id)

	return nil
}

func (u *User) Update() error {
	if u.Id == 0 {
		return errors.New("Cannot update non-existing record")
	}

	var stmt *sql.Stmt
	var err error

	if u.Password != "" {
		hpass, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		if err != nil {
			return err
		}
		u.Password = string(hpass)

		stmt, err = db.Prepare("UPDATE `users`" +
		"SET email=?, password=?, fullname=?, address=?, " +
		"phone=?, google_api=? WHERE id=?")
	} else {
		stmt, err = db.Prepare("UPDATE `users`" +
		"SET email=?, fullname=?, address=?, " +
		"phone=?, google_api=? WHERE id=?")
	}

	if err != nil {
		return err
	}

	gapi := "0"
	if u.GoogleAPI {
		gapi = "1"
	}

	if u.Password != "" {
		_, err = stmt.Exec(u.Email, u.Password, u.FullName, u.Address,
		u.Phone, gapi, u.Id)
	} else {
		_, err = stmt.Exec(u.Email, u.FullName, u.Address, u.Phone,
			gapi, u.Id)
	}

	if err != nil {
		return err
	}

	return nil
}

func GetUserByEmail(email string) (*User, error) {
	u := &User{}
	row := db.QueryRow(fmt.Sprintf("SELECT id, email, password, "+
		"fullname, address, phone "+
		"FROM `users` "+
		"WHERE email='%s' AND google_api='0'"+
		"LIMIT 1", email))

	err := row.Scan(&u.Id, &u.Email, &u.Password, &u.FullName, &u.Address, &u.Phone)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return u, nil
}

func GetUserByEmailPass(email string) (*User, error) {
	u := &User{}

	row := db.QueryRow(fmt.Sprintf("SELECT id, email, password, "+
		"fullname, address, phone "+
		"FROM `users` "+
		"WHERE email='%s' AND google_api ='0'" +
		"LIMIT 1", email))

	err := row.Scan(&u.Id, &u.Email, &u.Password, &u.FullName, &u.Address, &u.Phone)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return u, nil
}

func GetUserByEmailGoogle(email string) (*User, error) {
	u := &User{}

	row := db.QueryRow(fmt.Sprintf("SELECT id, email, password, "+
		"fullname, address, phone, google_api "+
		"FROM `users` "+
		"WHERE email='%s' AND google_api='1' "+
		"LIMIT 1", email))

	err := row.Scan(&u.Id, &u.Email, &u.Password, &u.FullName, &u.Address,
		&u.Phone, &u.GoogleAPI)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return u, nil
}

func GetUserById(id int) (*User, error) {
	u := &User{}
	row := db.QueryRow(fmt.Sprintf("SELECT id, email, password, "+
		"fullname, address, phone, google_api "+
		"FROM `users` "+
		"WHERE id=%d "+
		"LIMIT 1", id))

	var gapi string
	err := row.Scan(&u.Id, &u.Email, &u.Password, &u.FullName, &u.Address,
		&u.Phone, &gapi)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	if gapi == "1" {
		u.GoogleAPI = true
	}
	return u, nil
}
