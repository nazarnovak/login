$(document).ready(function() {
  gapi.load('auth2', function() {
    gapi.auth2.init();
  });
});

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();

    $.post("/auth/google", $.param({"email": profile.getEmail(), "fullname": profile.getName(), "googleapi": true}), function(response) {
        if(response.id) {
            window.location.href = "/setup";
        }
    },'json');
}

function signOut() {
console.log("signout");
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
}