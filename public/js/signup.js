(function($) {
    'use strict';

    $(document).ready(function() {
        var email = $('#email');
        var pass = $('#pass');
        var errorDiv = $('#error');

        function showError(text) {
            errorDiv.css('visibility','visible');
            errorDiv.html(text);
        }

        $("form").submit(function() {
            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if(response.id) {
                    window.location.href = "/setup";
                }

                showError(response.error);
            },'json');

            return false;
       });


        $('#signup').click(function() {
            var form = $(this).closest("form");

            errorDiv.css('visibility','hidden');
            errorDiv.html("&nbsp;");

            if(email.val() === "" || pass.val() === "") {
                showError("Email/pass must not be empty");
                return false;
            }

            var emailPat = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{1,4}$/;
            if(!emailPat.test(email.val())) {
                showError("Bad email");
                return false;
            }

            if(pass.val().length < 4) {
                showError("Password must be at least 4 characters");
                return false;
            }

            if(!$('form')[0].checkValidity()) {
                return false;
            }
        })
    })
}(jQuery))
