(function($) {
    'use strict';

    $(document).ready(function() {
        var pass = $('#pass');
        var errorDiv = $('#error');

        function showError(text) {
            errorDiv.css('visibility','visible');
            errorDiv.html(text);
        }

        $("form").submit(function() {
            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if(response.error === "") {
                    window.location.href = "/";
                }

                showError(response.error);
            },'json');

            return false;
       });

        $('#reset').click(function(){
            errorDiv.css('visibility','hidden');
            errorDiv.html("&nbsp;");

            if(pass.val() === "") {
                showError("Password must not be empty");
                return false;
            }

            if(pass.val().length < 4) {
                showError("Password must be at least 4 characters");
                return false;
            }

            if(!$('form')[0].checkValidity()) {
                return false;
            }
        })
    })
}(jQuery))