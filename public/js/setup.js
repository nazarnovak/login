(function($) {
    'use strict';

    $(document).ready(function() {
        var fullname = $('#fullname');
        var address = $('#address');
        var email = $('#email');
        var phone = $('#phone');
        var errorDiv = $('#error');

        function showError(text) {
            errorDiv.css('visibility','visible');
            errorDiv.html(text);
        }

        $("form").submit(function() {
            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if(response.id) {
                    window.location.href = "/profile";
                }

                showError(response.error);
            },'json');

            return false;
       });

        $('#setup').click(function(){
            errorDiv.css('visibility','hidden');
            errorDiv.html("&nbsp;");

            if(
                fullname.val() === "" ||
                address.val() === "" ||
                email.val() === "" ||
                email.val() === ""
            ) {
                showError("Fields must not be empty");
                return false;
            }

            var emailPat = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{1,4}$/;
            if(!emailPat.test(email.val())) {
                showError("Bad email");
                return false;
            }

            if(!$('form')[0].checkValidity()) {
                return false;
            }
        })
        $('#logout').click(function(e) {
            var href = $(this).attr("href");

            e.preventDefault();
            signOut();

            window.location.href = href;
        });
    })
}(jQuery))