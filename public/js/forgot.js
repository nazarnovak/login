(function($) {
    'use strict';

    $(document).ready(function() {
        var email = $('#email');
        var errorDiv = $('#error');

        function showError(text) {
            errorDiv.css('visibility','visible');
            errorDiv.html(text);
        }

        $("form").submit(function() {
            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if(response.error === "") {
                    window.location.href = "/";
                }
                showError(response.error);
            },'json');

            return false;
       });

        $('#forgot').click(function(){
            errorDiv.css('visibility','hidden');
            errorDiv.html("&nbsp;");

            if(email.val() === "") {
                showError("Email must not be empty");
                return false;
            }

            var emailPat = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{1,4}$/;
            if(!emailPat.test(email.val())) {
                showError("Bad email");
                return false;
            }

            if(!$('form')[0].checkValidity()) {
                return false;
            }
        })
    })
}(jQuery))