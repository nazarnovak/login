(function($) {
    'use strict';

    $(document).ready(function() {
        var email = $('#email');
        var pass = $('#pass');
        var errorDiv = $('#error');

        function showError(text) {
            errorDiv.css('visibility','visible');
            errorDiv.html(text);
        }

        $("form").submit(function() {
            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if(response.error === "") {
                    window.location.href = "/setup";
                }

                showError(response.error);
            },'json');

            return false;
       });

        $('#login').click(function(){
            errorDiv.css('visibility','hidden');
            errorDiv.html("&nbsp;");

            if(email.val() === "" || pass.val() === "") {
                showError("Email/pass must not be empty");
                return false;
            }

            if(!$('form')[0].checkValidity()) {
                return false;
            }
        })
    })
}(jQuery))